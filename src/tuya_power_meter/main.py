import time
import tinytuya
import configargparse
from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY

p = configargparse.ArgParser()
p.add('-p', '--port', default=9182, type=int,
      env_var='TPM_PORT', help='port to serve data')
p.add('-n', '--device_name', required=True, type=str,
      env_var='TPM_DEVICE_NAME', help='human readable name for the device')
p.add('-d', '--device_id', required=True, type=str,
      env_var='TPM_DEVICE_ID', help='device identifier')
p.add('-i', '--device_ip', required=True, type=str,
      env_var='TPM_DEVICE_IP', help='device ip address')
p.add('-k', '--local_key', required=True, type=str,
      env_var='TPM_LOCAL_KEY', help='secret key to read data')
options = p.parse_args()


class TPMStats():
    prefix = 'tpm_exporter'

    def __init__(self):
        pass

    def collect(self):
        try:
            d = tinytuya.OutletDevice(options.device_id, address=options.device_ip, version='3.3', local_key=options.local_key)
            d.updatedps([18,19,20])
            data = d.status()['dps']

            v = GaugeMetricFamily(f'{self.prefix}_voltage', f'voltage in Volts',
                                labels=['device_id'])
            v.add_metric([options.device_name], float(data['20'])/10)
            yield v 

            v = GaugeMetricFamily(f'{self.prefix}_current', f'current in Amps',
                                labels=['device_id'])
            v.add_metric([options.device_name], float(data['18'])/1000)
            yield v   

            v = GaugeMetricFamily(f'{self.prefix}_power', f'power in Watts',
                                labels=['device_id'])
            v.add_metric([options.device_name], float(data['19'])/10)
            yield v
        except:
            yield None


def main():
    REGISTRY.register(TPMStats())
    start_http_server(options.port)
    # sleep indefenetly
    while True:
        time.sleep(100)

if __name__ == "__main__":
    main()
