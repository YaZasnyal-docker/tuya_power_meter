FROM python:3-slim
WORKDIR /app
COPY . .
RUN pip install .
EXPOSE 9182/tcp
ENTRYPOINT ["tuya_power_meter"]
